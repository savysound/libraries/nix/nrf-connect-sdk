{
  lib,
  python3,
  gitlint,
  callPackage,
  pyproject,
  sdk
}:

let
  python = python3.override {
    self = python;
    packageOverrides = f: p: rec {
      inherit gitlint;

      clang-format = callPackage ./pkgs/python/clang-format.nix { };
      devicetree = callPackage ./pkgs/python/devicetree.nix { };
      svada = callPackage ./pkgs/python/svada.nix { };
      nrf-regtool = callPackage ./pkgs/python/nrf-regtool.nix { inherit devicetree svada; };
    };
  };

  nrf-proj = pyproject.lib.project.loadRequirementsTxt {
    requirements = "${sdk}/nrf/scripts/requirements.txt";
  };

  bootstrap-proj = pyproject.lib.project.loadRequirementsTxt {
    requirements = "${sdk}/bootloader/mcuboot/scripts/requirements.txt";
  };

  zephyr-proj = pyproject.lib.project.loadRequirementsTxt {
    requirements = "${sdk}/zephyr/scripts/requirements.txt";
  };

  invalidConstraints = nrf-proj.validators.validateVersionConstraints { inherit python; }
    // bootstrap-proj.validators.validateVersionConstraints { inherit python; }
    // zephyr-proj.validators.validateVersionConstraints { inherit python; };

in python.withPackages (nrf-proj.renderers.withPackages { 
  inherit python;
  extraPackages = (bootstrap-proj.renderers.withPackages {
    inherit python;
    extraPackages = (zephyr-proj.renderers.withPackages {
      inherit python;
      extraPackages = (ps: [ ps.nrf-regtool ps.west ]);
    });
  });
})
