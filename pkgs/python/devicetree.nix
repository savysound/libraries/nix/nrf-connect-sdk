{ python3Packages, fetchPypi }:

python3Packages.buildPythonPackage rec {
  pname = "devicetree";
  version = "0.0.2";

  doCheck = false;
  dontStrip = true;

  propagatedBuildInputs = with python3Packages; [ pyyaml ];

  src = fetchPypi {
    pname = "devicetree";
    inherit version;
    hash = "sha256-4bHoTmZwXFGQ3dfqq3JC1JSNSt0yC9UXrEdm80zpwMY=";
  };
}