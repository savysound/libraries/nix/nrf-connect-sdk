{ python3Packages, fetchPypi }:

python3Packages.buildPythonPackage rec {
  pname = "svada";
  version = "2.0.2";
  format = "pyproject";

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-C5NNUEjGYg7cLpkVVCNreMJtFAxzsW781faKvQDL6Vk=";
  };

  doCheck = false;
  dontStrip = true;

  nativeBuildInputs = with python3Packages; [
    pythonRelaxDepsHook
    setuptools 
    setuptools_scm 
  ];
  propagatedBuildInputs = with python3Packages; [ lxml numpy typing-extensions ];

  pythonRelaxDeps = true;
}