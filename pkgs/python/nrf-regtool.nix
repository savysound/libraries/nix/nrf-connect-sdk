{ python3Packages, fetchPypi, devicetree, svada }:

python3Packages.buildPythonPackage rec {
  pname = "nrf-regtool";
  version = "5.2.0";
  format = "pyproject";

  doCheck = false;
  dontStrip = true;

  nativeBuildInputs = with python3Packages; [ setuptools setuptools_scm ];
  propagatedBuildInputs = with python3Packages; [ click devicetree intelhex svada tomli more-itertools ];

  src = fetchPypi {
    pname = "nrf_regtool";
    inherit version;
    hash = "sha256-pKwGnZwOeb08FuD9y+1QI1zJRzFAmmwll6N0almTFMY=";
  };
}