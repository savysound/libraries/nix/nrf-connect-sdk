{ python3Packages, clang-tools_18, fetchPypi, fetchFromGitHub }:

python3Packages.buildPythonPackage rec {
  pname = "clang-format";
  version = "18.1.4";

  srcs = [
    (fetchPypi {
      pname = "clang_format";
      inherit version;
      hash = "sha256-37rugGvVX19VirrKRNzPfH7hMlqkwkf8tT9JrlUg9yY=";
    })
    (fetchFromGitHub {
      owner = "llvm";
      repo = "llvm-project";
      rev = "llvmorg-${version}";
      hash = "sha256-LyQEb4ZJXm2hkPOM9XITIploMT2VKIQWxUFio7SXrc0=";
    })
  ];

  # Copy only the relevant scripts to a build directory.  Made the directory structure what Nix
  # (setuptools) expects.
  postUnpack = ''
    mkdir clang_format
    cp clang_format-${version}/clang_format/__init__.py clang_format

    mkdir clang_format/git_clang_format
    cp source/clang/tools/clang-format/git-clang-format clang_format/git_clang_format/__init__.py

    mkdir clang_format/clang_format_diff
    cp source/clang/tools/clang-format/clang-format-diff.py clang_format/clang_format_diff/__init__.py
  '';

  sourceRoot = ".";

  # Patch the python for the path to the Nix clang-format executable.
  postPatch = ''
    substituteInPlace clang_format/__init__.py \
      --replace 'return os.path.join(os.path.dirname(__file__), "data", "bin", name)' 'return "${clang-tools_18}/bin/clang-format"'
  '';

  # We will fully ignore the build for this package.  It pulls in LLVM and builds
  # it within the pyproject.toml build just for the clang-format executable.  We
  # already have this executable easily available in nixpkgs.  As such, the build
  # is being replaced by a simple setup.py file injected by this derivation.
  preBuild = ''
    cat > setup.py << EOF
    from setuptools import setup

    setup(
      name='clang-format',
      packages=['clang_format', 'clang_format.git_clang_format', 'clang_format.clang_format_diff'],
      version='${version}',
      entry_points={
        'console_scripts': [
          'clang-format=clang_format:clang_format',
          'git-clang-format=clang_format.git_clang_format:main',
          'clang-format-diff=clang_format.clang_format_diff:main'
        ]
      },
    )
    EOF
  '';

  doCheck = false;

  nativeBuildInputs = with python3Packages; [ setuptools ];

  propagatedBuildInputs = [ clang-tools_18 ];
}
