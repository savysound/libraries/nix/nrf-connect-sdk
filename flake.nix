{
  description = "nRF Connect SDK for Nordic Semiconductor products";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    
    pyproject.url = "github:nix-community/pyproject.nix";
    pyproject.inputs.nixpkgs.follows = "nixpkgs";

    sdk-nrf.url = "git+https://gitlab.com/savysound/libraries/nix/sdk-nrf";
    sdk-nrf.inputs.nixpkgs.follows = "nixpkgs";
    sdk-nrf.inputs.flake-utils.follows = "flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, pyproject, sdk-nrf }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = (import nixpkgs { inherit system; }).pkgs;
        sdk = sdk-nrf.packages.${system}.default;
      in
      {
        packages = {
          default = sdk;
          pythonEnv = pkgs.callPackage ./python.nix { inherit pyproject sdk; };
        };
      }
    );
}

